### Dependencies
Ionic
[window.plugins.toast](https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin)
[FileTransfer](git://github.com/jvpsousa/hubx-cordova-plugin-file-transfer)
[Cordova background mode](https://github.com/katzer/cordova-plugin-background-mode)
[cordova-plugin-camera](https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-camera/)
[cordova-plugin-file](https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html)
[uiCropper](https://github.com/CrackerakiUA/ui-cropper)

```html
<link href="lib/ng-img-crop/compile/unminified/ui-cropper.css" rel="stylesheet" />
<script src="lib/ui-cropper/compile/unminified/ui-cropper.js"></script>
<script src="lib/upload-photo-component/upload-photo-component.js"></script>
```
### Insert module
```javascript
angular.module('myModule', ['ionicUpload'])
```

### Use
```html
<ionic-upload ng-model="myModel" options="options"></ionic-upload>
```
### options
```javascript
{
	titleActionSheet: 'STRING',
	actionSheetCameraText: 'STRING',
	actionSheetGaleryText: 'STRING',
	actionSheetCancelText: 'STRING',
	uploadUrl: 'STRING',
	type: 'STRING',
	crop: {
		headerText: 'STRING',
		confirmText: 'STRING',
		width: 'INT',
		height: 'INT',
		areaType: 'STRING',
		ratio: 'FLOAT'
	}
	backgroundMode: {
		title: 'STRING',
		text: 'STRING',
		icon: 'STRING',
		color: 'STRING',
		resume: 'BOOLEAN',
		hidden: 'BOOLEAN'
	}
}
```
