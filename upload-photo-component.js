(function() {
	'use strict'

	angular.module('ionicUpload', ['ionic', 'uiCropper'])
		.directive('uploadButton', function() {
			return {
				restrict: 'A',
				require: '^ionicUpload',
				link: function(scope, element, attrs) {
					element[0].addEventListener('click', function(e) {
						scope.showActionSheet()
					})
				}
			}
		})
		.directive('ionicUpload', function() {
			return {
				restrict: 'E',
				require: ['ngModel'],
				scope: {
					ngModel: '=',
					options: '='
				},
				template: '<div class="box-upload-image text-center">' +
					'<div class="rounded-icon" upload-button="upload-button" ng-if="!showImage()">' +
					'<img src="assets/img/upload-image.png"/></div>' +
					'<div class="box-current-image" upload-button="upload-button" ng-if="showImage()">' +
					'<div class="rounded-icon"><img ng-src="{{imageUrl}}"/></div>' +
					'<div class="box-change-photo"><i class="ion-ios-plus-empty"></i></div>' +
					'</div>' +
					'</div>',
				link: function() {},
				controller: function($scope, $ionicActionSheet, $ionicLoading, $rootScope,
					$http, $ionicModal) {
					var modalCrop
					var cropOptions = $scope.options.crop || {}
					var headerText = cropOptions.headerText || 'Cortar Imagem'
					var confirmText = cropOptions.confirmText || 'Cortar'

					$scope.options.type = $scope.options.type || 'UNIQUE'
					$scope.myCroppedImage = ''
					$scope.imageToCrop = ''
					$scope.cropAreaType = cropOptions.areaType || 'square'
					$scope.cropRatio = cropOptions.ratio || 1
					$scope.resultImgSize = {
						w: cropOptions.width || 400,
						h: cropOptions.height || 400
					}

					if ($scope.options.type === 'UNIQUE') {
						$scope.ngModel = $scope.ngModel || ''
						$scope.imageUrl = $scope.options.uploadUrl + '/' +
							angular.copy($scope.ngModel)
					} else {
						$scope.ngModel = $scope.ngModel || []
					}

					if (window.cordova && cordova.plugins && cordova.plugins.backgroundMode) {
						cordova.plugins.backgroundMode.enable()
						var backgroundModeOptions = $scope.options.backgroundMode || {}

						cordova.plugins.backgroundMode.setDefaults({
							title: backgroundModeOptions.title || 'Em espera',
							text: backgroundModeOptions.text || 'Em espera',
							icon: backgroundModeOptions.icon || 'icon',
							color: backgroundModeOptions.color || '#3984AF',
							resume: backgroundModeOptions.resume || true,
							hidden: backgroundModeOptions.hidden || false
						})
					}

					$rootScope.$on('$ionicView.beforeLeave', function(event) {
						if (window.cordova && cordova.plugins && cordova.plugins.backgroundMode) {
							cordova.plugins.backgroundMode.disable()
						}
					})

					$scope.showImage = function() {
						if (!$scope.ngModel && $scope.options.type === 'UNIQUE') {
							return false
						} else if ($scope.ngModel && $scope.options.type === 'UNIQUE') {
							return true
						} else if ($scope.options.type === 'ARRAY') {
							return false
						}
					}

					$scope.showActionSheet = function() {
						$ionicActionSheet.show({
							titleText: $scope.options.titleActionSheet || 'Escolher foto',
							buttons: [{
								text: $scope.options.actionSheetCameraText || 'Tirar foto'
							}, {
								text: $scope.options.actionSheetGaleryText ||
									'Escolher foto da galeria'
							}],
							cancelText: $scope.options.actionSheetCancelText || 'Cancelar',
							buttonClicked: function(index) {
								if (index == 0) _showCamera()
								if (index == 1) _showGalery()
								return true
							}
						})
					}

					$scope.confirmCrop = function(base64) {
						_uploadImage(base64)
					}

					$scope.cancelCrop = function() {
						modalCrop.hide()
					}

					var _showCamera = function() {
						var camera = navigator.camera
						var cameraOptions = ionic.Platform.isAndroid() ?
							_getAndroidCameraOptions() :
							_getIOSCameraOptions()

						_startCamera(camera, cameraOptions)
					}

					var _getDefaultCameraOrGalleryOptions = function() {
						return {
							quality: 50,
							saveToPhotoAlbum: true
						}
					}

					var _getAndroidCameraOptions = function() {
						var cameraOptions = _getDefaultCameraOrGalleryOptions()

						cameraOptions.destinationType = Camera.DestinationType.DATA_URL
						cameraOptions.sourceType = Camera.PictureSourceType.CAMERA
						cameraOptions.correctOrientation = true
						cameraOptions.allowEdit = false
						cameraOptions.popoverOptions = CameraPopoverOptions

						return cameraOptions
					}

					var _getIOSCameraOptions = function() {
						var cameraOptions = _getDefaultCameraOrGalleryOptions()

						cameraOptions.destinationType = Camera.DestinationType.DATA_URL
						cameraOptions.sourceType = Camera.PictureSourceType.CAMERA
						cameraOptions.allowEdit = false
						cameraOptions.correctOrientation = true
						cameraOptions.popoverOptions = CameraPopoverOptions

						return cameraOptions
					}

					var _startCamera = function(camera, cameraOptions) {
						$ionicLoading.show()
						camera.getPicture(function(pictureURI) {
							_successCamera(pictureURI)
						}, function(cameraError) {
							$ionicLoading.hide()
							console.log(cameraError)
							if (cameraError.indexOf('cancelled') == -1) {
								window.plugins.toast.show('Erro ao abrir câmera.', 'short',
									'bottom', null, null)
							}
						}, cameraOptions)
					}

					var _showGalery = function() {
						var gallery = navigator.camera
						var galleryOptions = ionic.Platform.isAndroid() ?
							_getAndroidGalleryOptions() :
							_getIOSGalleryOptions()

						_startGallery(gallery, galleryOptions)
					}

					var _getAndroidGalleryOptions = function() {
						var galleryOptions = _getDefaultCameraOrGalleryOptions()
						galleryOptions.destinationType = Camera.DestinationType.DATA_URL
						galleryOptions.correctOrientation = true
						galleryOptions.allowEdit = false
						galleryOptions.sourceType = Camera.PictureSourceType.SAVEDPHOTOALBUM
						galleryOptions.popoverOptions = CameraPopoverOptions

						return galleryOptions
					}

					var _getIOSGalleryOptions = function() {
						var galleryOptions = _getDefaultCameraOrGalleryOptions()
						galleryOptions.allowEdit = false
						galleryOptions.correctOrientation = true
						galleryOptions.destinationType = Camera.DestinationType.DATA_URL
						galleryOptions.sourceType = Camera.PictureSourceType.SAVEDPHOTOALBUM
						galleryOptions.popoverOptions = CameraPopoverOptions

						return galleryOptions
					}

					var _startGallery = function(gallery, galleryOptions) {
						$ionicLoading.show()
						gallery.getPicture(function(pictureURI) {
							_successCamera(pictureURI)
						}, function(galleryError) {
							$ionicLoading.hide()
							console.log(galleryError)
							if (galleryError.indexOf('cancelled') == -1) {
								window.plugins.toast.show('Erro ao abrir galeria.', 'short',
									'bottom', null, null)
							}
						}, galleryOptions)
					}

					var _successCamera = function(pictureURI) {
						pictureURI = 'data:image/png;base64,' + pictureURI

						var opt = {
							scope: $scope,
							animation: 'fadeIn'
						}

						$scope.imageToCrop = pictureURI
						$scope.$apply()

						var template = getModalCropTemplate(headerText, confirmText)

						modalCrop = $ionicModal.fromTemplate(template, opt)
						modalCrop.show()
						setTimeout(function() {
							$scope.$apply()
							$ionicLoading.hide()
						})
					}

					var _uploadImage = function(image) {
						$ionicLoading.show()

						$http.post($scope.options.uploadUrl, {
							media: image
						}).then(function(success) {
							$ionicLoading.hide()
							if ($scope.options.type === 'UNIQUE') {
								$scope.ngModel = success.data.fileName
							} else {
								$scope.ngModel.push(success.data.fileName)
							}
							$scope.imageUrl = $scope.options.uploadUrl + '/' + $scope.ngModel
							modalCrop.hide()
						}).catch(function(err) {
							console.log(err)
							$ionicLoading.hide()
							window.plugins.toast.show('Erro ao fazer upload de imagem.',
								'short', 'bottom', null, null)
						})
					}

					var getModalCropTemplate = function(headerText, confirmText) {
						return '<ion-modal-view class="crop-modal">' +
							'<ion-header-bar>' +
							'<button class="button button-clear button-primary">Cancelar</button>' +
							'<h1 class="title">' + headerText + '</h1>' +
							'<button ng-click="cancelCrop()" class="button button-clear button-primary btn-cancel"><i class="ion-ios-close-empty"></i></button>' +
							'</ion-header-bar>' +
							'<ion-content>' +
							'<div class="wrapper">' +
							'<div class="cropArea">' +
							'<ui-cropper image="imageToCrop" aspect-ratio="cropRatio" area-type="{{cropAreaType}}" result-image-size="resultImgSize" result-image="myCroppedImage"></ui-cropper>' +
							'</div>' +
							'<div class="box-buttons">' +
							'<button class="findi-btn" ng-click="confirmCrop(myCroppedImage)">' +
							confirmText +
							'</button>' +
							'</div>' +
							'</div>' +
							'</ion-content>' +
							'</ion-modal-view>'
					}
				}
			}
		})
})()
